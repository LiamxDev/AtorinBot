const mysql = require('mysql')
let pool
module.exports = {
    getPool: function () {
        if (pool) return pool
        pool = mysql.createPool({
            host: process.env.mysqlhost,
            user: process.env.mysqluser,
            password: process.env.mysqlpass,
            database: process.env.mysqldb
        })
        return pool
    }
}