const Discord = require('discord.js')
const db = require('./../database.js')
let pool = db.getPool()
module.exports = exports = function (extras) {
  return new Promise(function (resolve, reject) {
    let langdir = __dirname.replace('commands', 'languages/')
    const lang = require(langdir + extras[3] + '.js')
    let message = extras[0]
    let client = extras[1]
    let args = extras[2]
    if (!message.member.hasPermission('ADMINISTRATOR')) {
        resolve([lang.lang.permission + 'ADMINISTRATOR'])
        return
    }
    if (args.length === 0) {
        resolve([lang.delmsg.usage])
        return
    }
    if (!/^[^a-zA-Z]*$/.test(args[0])) {
        resolve([lang.delmsg.usage])
        return
      }
    message.channel.fetchMessages({ limit: args[0]})
    .then(messages => {
        message.channel.bulkDelete(messages)
        pool.query(`SELECT logs FROM servers WHERE server = ${messages.last().guild.id}`, function (err, result) {
            if (err) throw err
            if (typeof messages.last().guild.channels.get(result[0].logs) === 'undefined') return
            let msgList = ''
            for (let index = 0; index < messages.array().length; index++) {
                let time = new Date(messages.array()[index]['createdTimestamp'])
                msgList += `${time.getHours()}:${time.getMinutes()}|`
                msgList += `<@${messages.array()[index]['author']['id']}>: `
                msgList += `${messages.array()[index]['content']}\n`
            }
            const embed = new Discord.RichEmbed()
            let channelId = messages.last().guild.channels.find('name', messages.last().channel.name)
            embed.setAuthor('Usunięte wiadomości', client.user.avatarURL)
            embed.setDescription(`${msgList}\nKanał: ${channelId}\nData usunięcia: ${new Date(Date.now()).toLocaleString()}`)
            embed.setColor('RED')
            embed.setThumbnail(message.guild.iconURL)
            embed.setFooter('AtorinBot by LiamxDev')
            messages.last().guild.channels.get(result[0].logs).send(embed)
        })
    }).catch(console.error)
  })
}
