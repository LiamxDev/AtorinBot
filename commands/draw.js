const coins = require('../utils/coins');
const Discord = require('discord.js');
module.exports = exports = function (extras) {
    return new Promise(function(resolve){
        let langdir = __dirname.replace('commands', 'languages/');
        const lang = require(langdir + extras[3] + '.js');
        let message = extras[0];
        let args = extras[2];
        if (args.length === 0){
            resolve([lang.draw.usage]);
        }
        if (Math.sign(args[0]) === -1) {
            resolve([lang.draw.usage]);
        }
        coins.getCoins(message.author.id, function (count) {
            if (count >= args[0]){
                coins.removeCoins(message.author.id, args[0], function (success) {
                    if (success){
                        const embed = new Discord.RichEmbed();
                        embed.setAuthor('🎰 ' + lang.draw.title, extras[1].user.avatarURL);
                        embed.addField('Postawione:', args[0]);
                        const number = Math.floor(Math.random() * 100);
                        switch (true) {
                            case number <= 20:
                                embed.addField('Wynik:', 'Przegrana');
                                embed.addField('Stan konta:', count - args[0]);
                                embed.setColor("#ff0000");
                                resolve([embed]);
                                break;
                            case number >= 20 && number <= 70:
                                coins.addCoin(message.author.id, args[0]);
                                embed.addField('Wynik:', 'Remis');
                                embed.addField('Stan konta:', count);
                                embed.setColor('#ffff00');
                                resolve([embed]);
                                break;
                            case number >= 70:
                                coins.addCoin(message.author.id, args[0] * 2);
                                embed.addField('Wynik:', 'Wygrana');
                                embed.addField('Stan konta:', Number.parseInt(count) + Number.parseInt(args[0]));
                                embed.setColor('#00ff00');
                                resolve([embed]);
                                break;
                        }
                    }
                })
            } else {
                resolve([lang.draw.coins]);
            }
        });
    })
};