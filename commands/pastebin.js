const pastebin = require('better-pastebin')
module.exports = exports = function (extras) {
  return new Promise(function (resolve, reject) {
    let langdir = __dirname.replace('commands', 'languages/')
    const lang = require(langdir + extras[3] + '.js')
    if (extras[2].length === 0) {
      resolve([lang.pastebin.usage])
      return
    }
    pastebin.setDevKey(process.env.pastebin)
    pastebin.create({contents: extras[0].content.replace('&pastebin', '')}, function (success, data) {
      if (success) {
        resolve([`<@${extras[0].author.id}>` + lang.pastebin.success + `${data}`])
      } else {
        console.log(data)
        resolve([lang.pastebin.fail])
      }
    })
  })
}
