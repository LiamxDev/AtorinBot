const Discord = require('discord.js')
module.exports = exports = function (extras) {
  return new Promise(function (resolve, reject) {
    let msg = extras[0]
    let client = extras[1]
    let args = extras[2]
    let langdir = __dirname.replace('commands', 'languages/')
    const lang = require(langdir + extras[3] + '.js')
    if (args.length === 0) {
      resolve([lang.advert.usage])
      return
    }
    if (!msg.member.hasPermission('ADMINISTRATOR')) {
      resolve([lang.advert.permission + 'ADMINISTRATOR'])
      return
    }
    const embed = new Discord.RichEmbed()
    embed.setAuthor('📢 ' + lang.advert.title, client.user.avatarURL)
    embed.setDescription(msg.content.replace('&advert ', ''))
    embed.setColor('RED')
    embed.setFooter('AtorinBot by LiamxDev')
    let reactions = ['👍', '😂', '😮', '😢', '😡', '😍']
    resolve([embed, reactions])
  })
}
