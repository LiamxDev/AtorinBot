const Discord = require('discord.js')
module.exports = exports = function (extras) {
  return new Promise(function (resolve, reject) {
    let langdir = __dirname.replace('commands', 'languages/')
    const lang = require(langdir + extras[3] + '.js')
    if (extras[2].length === 0) {
      resolve([lang.avatar.usage])
      return
    }
    if (typeof extras[0].mentions.users.first() === 'undefined') {
      resolve([lang.avatar.usage])
      return
    }
    const embed = new Discord.RichEmbed()
    embed.setAuthor('🤳 ' + lang.avatar.title + extras[0].mentions.users.first().username, extras[1].user.avatarURL)
    embed.setImage(extras[0].mentions.users.first().avatarURL)
    embed.setFooter('AtorinBot by LiamxDev')
    resolve([embed])
  })
}
