const Discord = require('discord.js')
module.exports = exports = function (extras) {
  return new Promise(function (resolve, reject) {
    let langdir = __dirname.replace('commands', 'languages/')
    const lang = require(langdir + extras[3] + '.js')
    if (extras[2].length === 0) {
      resolve([lang.calc.usage])
      return
    }
    if (extras[2].length === 0 || isNaN(extras[2][0]) || typeof extras[2][1] === 'undefined' || isNaN(extras[2][2])) {
      resolve([lang.calc.usage])
      return
    }
    if (!/^[^a-zA-Z]*$/.test(extras[0].content.replace('&calc ', ''))) {
      resolve([lang.calc.usage])
      return
    }
    const embed = new Discord.RichEmbed()
    embed.setAuthor('➗ ' + lang.calc.title, extras[1].user.avatarURL)
    embed.addField(lang.calc.data, extras[0].content.replace('&calc', ''))
    embed.addField(lang.calc.result, eval(extras[0].content.replace('&calc', '')))
    embed.setFooter('AtorinBot by LiamxDev')
    resolve([embed])
  })
}
